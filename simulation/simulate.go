package simulation

import "gitlab.com/earl-grey-imperial-hackathon/server-application/slot"

func Simulate(slot *slot.Slot, count uint32, bet uint32) (uint32, float32) {
	var totalReward uint32 = 0
	for i := 0; i < int(count); i++ {
		totalReward += slot.SimulateSpin(bet)
	}
	rtp := 100 * float32(totalReward) / float32(count*bet)
	return totalReward, rtp
}

func SimulateGoroutine(rewardChannel chan uint32, rtpChannel chan float32, slot *slot.Slot, count uint32, bet uint32) {
	singleReward, singleRtp := Simulate(slot, count, bet)
	rewardChannel <- singleReward
	rtpChannel <- singleRtp
}
