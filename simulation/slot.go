package simulation

import (
	"gitlab.com/earl-grey-imperial-hackathon/server-application/slot"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

type SlotYaml struct {
	Reels [][]struct {
		Symbol string `yaml:"symbol"`
		Count  int    `yaml:"count"`
	} `yaml:"reels"`
	Combos []struct {
		Symbols []string `yaml:"symbols"`
		Prize   int      `yaml:"prize"`
	} `yaml:"combos"`
}

func ParseSlot(path string) (slot.Slot, error) {
	var slotYaml SlotYaml
	yamlContent, err := ioutil.ReadFile(path)
	if err != nil {
		return slot.Slot{}, err
	}
	err = yaml.Unmarshal(yamlContent, &slotYaml)
	if err != nil {
		return slot.Slot{}, err
	}

	var reels []slot.Reel

	for _, yamlReel := range slotYaml.Reels {
		var symbolsWithCount []slot.SymbolWithCount
		for _, symbolWithCountYaml := range yamlReel {
			symbolsWithCount = append(symbolsWithCount, slot.NewSymbolWithCount(symbolWithCountYaml.Symbol, uint32(symbolWithCountYaml.Count)))
		}
		symbols := slot.StringArrayShuffle(slot.SymbolArrayUnpacker(symbolsWithCount))
		reels = append(reels, slot.NewReel(symbols))
	}

	var combos []slot.WinningCombination

	for _, yamlCombo := range slotYaml.Combos {
		combos = append(combos, slot.NewWinningCombination(yamlCombo.Symbols, uint32(yamlCombo.Prize)))
	}

	return slot.NewSlot(reels, combos), nil
}
