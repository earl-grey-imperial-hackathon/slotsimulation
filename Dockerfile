FROM golang:1.18 AS builder

WORKDIR /app

COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -o rtp-simulation main.go

FROM alpine:latest as production

WORKDIR /app

COPY --from=builder /app/rtp-simulation /usr/bin/
COPY --from=builder /app/input.yaml .

CMD ["rtp-simulation","-f","input.yaml"]
