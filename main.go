package main

import (
	"flag"
	log "github.com/sirupsen/logrus"
	"gitlab.com/earl-grey-imperial-hackathon/slotsimulation/simulation"
	"os"
	"time"
)

const (
	DefaultGoroutines      = 1
	DefaultBet             = 1
	DefaultSimulationCount = 10000
	DefaultYaml            = "slot.yaml"
)

func main() {

	scFlag := flag.Uint("c", DefaultSimulationCount, "number of simulations to run")
	betFlag := flag.Uint("b", DefaultBet, "bet value")
	grFlag := flag.Uint("g", DefaultGoroutines, "number of goroutines")
	pathFlag := flag.String("f", DefaultYaml, "path to slot yaml definition")

	flag.Parse()

	simulationCount := uint32(*scFlag)
	bet := uint32(*betFlag)
	goroutines := uint32(*grFlag)
	path := *pathFlag

	slot, err := simulation.ParseSlot(path)
	if err != nil {
		log.Errorf("Error %v\n", err.Error())
		os.Exit(1)
	}

	rewardChannel := make(chan uint32, goroutines)
	rtpChannel := make(chan float32, goroutines)

	timeNow := time.Now()

	var countsPerThread []uint32
	var allocated uint32 = 0
	for i := 0; i < int(goroutines)-1; i++ {
		toAllocate := simulationCount / goroutines
		countsPerThread = append(countsPerThread, toAllocate)
		allocated += toAllocate
	}
	countsPerThread = append(countsPerThread, simulationCount-allocated)
	log.Infof("Counts per thread %v", countsPerThread)
	for i := 0; i < int(goroutines); i++ {
		go simulation.SimulateGoroutine(rewardChannel, rtpChannel, &slot, countsPerThread[i], bet)
	}

	var reward uint32 = 0
	var rewardCount uint32 = 0
	var rtpSum float32 = 0
	var rtpCount uint32 = 0

	for {
		if rewardCount == goroutines && rtpCount == goroutines {
			break
		}

		select {
		case res := <-rewardChannel:
			reward += res
			rewardCount++
		case res := <-rtpChannel:
			rtpSum += res
			rtpCount++
		}

	}

	execTime := time.Since(timeNow)

	rtp := rtpSum / float32(rtpCount)

	log.Infof("Goroutines: %v , Number of simulations %v,Execution time : %vms", goroutines, simulationCount, execTime.Milliseconds())

	log.Infof("Reward %v , RTP %v", reward, rtp)
}
